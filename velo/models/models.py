# -*- coding: utf-8 -*-

from odoo import models, fields, api

class velo(models.Model):
    _name = 'velo.scooters'
    _description='Velo'

    name = fields.Char('Name')
    prix= fields.Float('Prix')
    date=fields.Date('Date')
    qte =fields.Char('Quantite')
    fournisseur=fields.Char('Fournisseur')

class scotters(models.Model):
    _inherit='repair.order'


    commission =fields.Float('Commission')
    partner_ids=fields.Many2one('res.partner','Partner')
